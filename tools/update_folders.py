import os
from pathdefs import docs, doc
from pathlib import Path
import shutil


def update_one(loc):
    os.chdir(os.path.join(doc))
    files = Path('.').glob('**/*')

    for f in files:
        src = os.path.join(doc,f)
        dst = os.path.join(docs,loc,f)
        if Path(src).is_dir():
            Path(dst).mkdir(exist_ok=True)
        if Path(src).is_file():
            if src.endswith('.py'): # hard-copy conf.py and add localization-related options
                shutil.copyfile(src, dst)
                with open(dst, 'a') as fin:
                    fin.write('\nlanguage = "{:s}"'.format(loc))
                    fin.write('\nlocale_dirs = ["../../translated/"]')
                    fin.write('\ngettext_compact = "siril-documentation"')
            else:
                srcrel = os.path.relpath(src, os.path.split(dst)[0])
                os.symlink(srcrel, dst)


def update_all():
    os.chdir(docs)
    dirs = Path('.').glob('*')
    all_locs=[]

    # updating all ./docs/* subfolders
    for d in dirs:
        loc = str(d)
        all_locs += '{:s} '.format(loc)
        print('Updating {:s}'.format(loc))
        shutil.rmtree(Path(docs).joinpath(loc))
        try:
            Path(docs).joinpath(loc).mkdir(exist_ok=True)
        except Exception as err:
            print(err)
        update_one(str(d))

if __name__ == "__main__":
    update_all()