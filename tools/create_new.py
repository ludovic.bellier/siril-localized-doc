import sys
from pathlib import Path
from pathdefs import docs, translated
from update_folders import update_one


txt = input("Code for the new language: ")

if len(txt) == 0:
    print("Check available language codes at https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language")
    sys.exit(0)

# txt='fr'
if Path(docs).joinpath(txt).is_dir():
    print("Language already exists")
    sys.exit(0)

try:
    Path(docs).joinpath(txt).mkdir()
except Exception as err:
    print(err)
    sys.exit(1)

try:
    Path(translated).joinpath(txt).mkdir(parents = True)
except Exception as err:
    print(err)
    sys.exit(1)

update_one(txt)
