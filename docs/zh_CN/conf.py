# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Siril'
copyright = '2023, Free-Astro Team'
author = 'Free-Astro Team'
release = '1.2.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

import sphinx_rtd_theme

extensions = [ 
    "sphinx.ext.autodoc",
    "sphinx.ext.autosectionlabel",
    "sphinx_rtd_theme",
    'sphinx.ext.mathjax',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinxcontrib.cairosvgconverter',
    'sphinx_copybutton',
    'sphinxcontrib.video',
]


autosectionlabel_prefix_document = True

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

todo_include_todos = True

smartquotes = False

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_static_path = ['_static']
html_extra_path = ['_static']
html_logo = "_images/org.free_astro.siril.svg"
html_theme_options = {
    "logo_only": True,
    "display_version": True,
}


# -- override for csv table formatting ---------------------------------------
def setup(app):

   app.add_css_file("custom.css")

language = "zh_CN"
locale_dirs = ["../../translated/"]
gettext_compact = "siril-documentation"